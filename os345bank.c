// os345bank.c
// ***********************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <assert.h>
#include "os345.h"
#include "os345dme.h"

// ***********************************************************************
// bank variables
extern Message messages[];			// process message buffers
extern Semaphore* msgReady[];		// message buffer semaphores

Account accounts[NUM_ACCOUNTS];	// bank accounts

// *****************************************************************************
// *****************************************************************************
// Process ATM commands
int atmTask(int argc, char* argv[])
{
   char code;
	int n, account, amount;
	Message newMsg;
   char buf[MAX_MESSAGE_SIZE];

//									BANK
//									(10) BANK
//
//						   myCLIPort		myATMPort		myDMEPort
//		CLI (0) <----> (1)	ATM1		(11) <------->	(21) DME1
//							(2)	ATM2		(12)				(22) DME2
//									....							  ....
//							(9)	ATM9		(19)				(29) DME9
//
	int myCLIPort = INTEGER(argv[1]);
	int myATMPort = myCLIPort + NUM_ATMS + 1;
	int myDMEPort = myATMPort + NUM_ATMS + 1;

	//printf("\natmTask%d  myCLIPort=%d, myATMPort=%d, myDMEPort=%d", curTask, myCLIPort, myATMPort, myDMEPort);

	while(1)
	{	// get message from CLI
		GETMESSAGE(CLI, myCLIPort, newMsg);
   	n = sscanf(newMsg.msg, "%c,%d,%d", &code, &account, &amount);
   	ATMDebug(("(%d->%d) '%s'", newMsg.from, newMsg.to, newMsg.msg));

		// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		// >>>>>>>>>>>>>> acquire bank here >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

		sprintf(buf, "A:ATM%d Bank Request", myCLIPort);
		POSTMESSAGE(myATMPort, myDMEPort, buf);
		GETMESSAGE(myDMEPort, myATMPort, newMsg);

		// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
   	// >>>>>>>>>>>>>>> transact business with bank >>>>>>>>>>>>>>>>>>

		// select account
		sprintf(buf, "s,%d", account);
   	POSTMESSAGE(myATMPort, BANK, buf);
		GETMESSAGE(BANK, myATMPort, newMsg);
   	if (newMsg.msg[0] == '+')
		{	ATMDebug(("Account %d selected", account));
   		if (code == 'n')							// new account
   		{	sprintf(buf, "n");
	   		POSTMESSAGE(myATMPort, BANK, buf);
				GETMESSAGE(BANK, myATMPort, newMsg);
      		if (newMsg.msg[0] == '+')
      		{	printf("\n  Account %04d created", account);
	   			// now make an initial deposit
					sprintf(buf, "d,%d", amount);
		   		POSTMESSAGE(myATMPort, BANK, buf);
					GETMESSAGE(BANK, myATMPort, newMsg);
		   		if (newMsg.msg[0] == '+') printf("\n  $%d deposited to %04d", amount, account);
				}
   		}

   		if (code == 'b')							// get account balance
   		{	sprintf(buf, "b");
	   		POSTMESSAGE(myATMPort, BANK, buf);
				GETMESSAGE(BANK, myATMPort, newMsg);
	   		if (newMsg.msg[0] == '+')
	   		{	n = sscanf(&newMsg.msg[2], "%d", &amount);
      			printf("\n  %04d balance = $%d", account, amount);
	   		}
   		}

   		if (code == 'd')							// make account deposit
   		{	sprintf(buf, "d,%d", amount);
	   		POSTMESSAGE(myATMPort, BANK, buf);
				GETMESSAGE(BANK, myATMPort, newMsg);
	   		if (newMsg.msg[0] == '+') printf("\n  $%d deposited to %04d", amount, account);
   		}

   		if (code == 't')							// get account totals
   		{	sprintf(buf, "t");
	   		POSTMESSAGE(myATMPort, BANK, buf);
				printf("\n  Acct   Balance");
				while (1)
				{
					GETMESSAGE(BANK, myATMPort, newMsg);
		   		if (newMsg.msg[0] == 'b') printf("\n  %s", &newMsg.msg[2]);
					else break;
				}
	   		if (newMsg.msg[0] == '+') printf("\n  End of Accounts");
   		}

   		if (code == 'w')							// withdraw from account
   		{	sprintf(buf, "w,%d", amount);
	   		POSTMESSAGE(myATMPort, BANK, buf);
				GETMESSAGE(BANK, myATMPort, newMsg);
	   		if (newMsg.msg[0] == '+') printf("\n  $%d withdrawn from %04d", amount, account);
   		}

   		if (code == 'x')							// delete account
   		{	sprintf(buf, "x");
	   		POSTMESSAGE(myATMPort, BANK, buf);
				GETMESSAGE(BANK, myATMPort, newMsg);
	   		if (newMsg.msg[0] == '+') printf("\n  %04d deleted", account);
   		}
		}
   	if (newMsg.msg[0] == '-') printf("\n  **Bank Error: %s", &newMsg.msg[2]);

		// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		// >>>>>>>>>>>>>> release bank here >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

		sprintf(buf, "R:ATM%d Bank Release", myCLIPort);
		POSTMESSAGE(myATMPort, myDMEPort, buf);
		GETMESSAGE(myDMEPort, myATMPort, newMsg);

		// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	} // while
	return 0;
} // end atmTask


// *****************************************************************************
int bankTask(int argc, char* argv[])
{
   int account = 0;				// current account
	Message newMsg;
   int i, n, data;
	char code;
   char buf[MAX_MESSAGE_SIZE];

	while(1)
	{
   	GETMESSAGE(ATMS, BANK, newMsg);		// wait for message
   	n = sscanf(newMsg.msg, "%c,%d", &code, &data);
		BANKDebug(("(%d->%d) '%s'", newMsg.from, newMsg.to, newMsg.msg));

   	if (code == 'b')							// account balance
   	{	for (i=0; i<NUM_ACCOUNTS; i++)
      	{	if (accounts[i].id == account)
         	{	BANKDebug(("%04d balance is $%d", account, accounts[i].balance));
					sprintf(buf, "+:%d balance", accounts[i].balance);
            	POSTMESSAGE(BANK, newMsg.from, buf);
            	break;
         	}
      	}
      	if (i == NUM_ACCOUNTS)
			{	sprintf(buf, "-:%04d Not Found", account);
      		POSTMESSAGE(BANK, newMsg.from, buf);
      	}
      	continue;
   	}

   	if (code == 'd')							// account deposit
   	{	for (i=0; i<NUM_ACCOUNTS; i++)
      	{	if (accounts[i].id == account)
         	{	accounts[i].balance += data;
					BANKDebug(("$%d deposited to account %04d", data, account));
					sprintf(buf, "+:%d deposited", data);
		      	POSTMESSAGE(BANK, newMsg.from, buf);
            	break;
				}
			}
      	if (i == NUM_ACCOUNTS)
			{	sprintf(buf, "-:%04d Not Found", account);
      		POSTMESSAGE(BANK, newMsg.from, buf);
      	}
      	continue;
   	}

   	if (code == 'n')							// new account
   	{	if (account == 9999)
			{	sprintf(buf, "-:Illegal Account %04d", account);
	      	POSTMESSAGE(BANK, newMsg.from, buf);
	      	continue;
			}
			for (i=0; i<NUM_ACCOUNTS; i++)
      	{	if (accounts[i].id == account)
         	{	BANKDebug(("Account %04d already defined", account));
					sprintf(buf, "-:%04d Already Defined", account);
					POSTMESSAGE(BANK, newMsg.from, buf);
            	break;
         	}
         	if (accounts[i].id == 0)
         	{	accounts[i].id = account;
            	accounts[i].balance = 0;
					BANKDebug(("New account %04d", account));
					sprintf(buf, "+:%04d defined", data);
		      	POSTMESSAGE(BANK, newMsg.from, buf);
            	break;
         	}
      	}
      	if (i == NUM_ACCOUNTS)
      	{	sprintf(buf, "-:Too Many Accounts");
      		POSTMESSAGE(BANK, newMsg.from, buf);
      	}
      	continue;
   	}

   	if (code == 's')							// select account
		{	if ((data >= 1000) && (data <= 9999))
      	{	account = data;
	      	BANKDebug(("Account %04d selected", account));
				sprintf(buf, "+:%04d selected", data);
         	POSTMESSAGE(BANK, newMsg.from, buf);
         	continue;
      	}
      	sprintf(buf, "-:Illegal Account %04d", data);
      	POSTMESSAGE(BANK, newMsg.from, buf);
      	continue;
		}

   	if (code == 't')							// account summary
		{	if (account == 9999)
      	{	for (i=0; i<NUM_ACCOUNTS; i++)
				{	if (accounts[i].id)
					{
			      	BANKDebug(("Account %04d = $%d", accounts[i].id, accounts[i].balance));
						sprintf(buf, "b:%04d = $%d", accounts[i].id, accounts[i].balance);
		         	POSTMESSAGE(BANK, newMsg.from, buf);
					}
				}
	      	sprintf(buf, "+:End of Accounts");
         	POSTMESSAGE(BANK, newMsg.from, buf);
         	continue;
      	}
      	sprintf(buf, "-:Illegal Account %04d", account);
      	POSTMESSAGE(BANK, newMsg.from, buf);
      	continue;
		}

   	if (code == 'w')							// withdraw from account
   	{	for (i=0; i<NUM_ACCOUNTS; i++)
      	{	if (accounts[i].id == account)
         	{	if (accounts[i].balance < data)
            	{	sprintf(buf, "-:NSF");
		         	POSTMESSAGE(BANK, newMsg.from, buf);
               	break;
            	}
            	accounts[i].balance -= data;
	         	BANKDebug(("$%d withdrawn from %04d", data, account));
					sprintf(buf, "+:%d deposited", data);
	         	POSTMESSAGE(BANK, newMsg.from, buf);
            	break;
         	}
      	}
      	if (i == NUM_ACCOUNTS)
			{	sprintf(buf, "-:%04d Not Found", account);
      		POSTMESSAGE(BANK, newMsg.from, buf);
      	}
      	continue;
   	}

   	if (code == 'x')							// delete account
   	{	for (i=0; i<NUM_ACCOUNTS; i++)
      	{	if (accounts[i].id == account)
         	{	if (accounts[i].balance != 0)
            	{	sprintf(buf, "-:Nonzero Balance");
		         	POSTMESSAGE(BANK, newMsg.from, buf);
               	break;
	            }
					accounts[i].id = 0;
      	      accounts[i].balance = 0;
	      	   BANKDebug(("Account %04d deleted", account));
					sprintf(buf, "+:%04d deleted", data);
	         	POSTMESSAGE(BANK, newMsg.from, buf);
            	break;
         	}
      	}
      	if (i == NUM_ACCOUNTS)
			{	sprintf(buf, "-:%04d Not Found", account);
      		POSTMESSAGE(BANK, newMsg.from, buf);
      	}
      	continue;
   	}

		printf("\n  **Bank: Illegal message from %04d: %s", newMsg.from, newMsg.msg);
   	sprintf(buf, "-:Illegal message");
   	POSTMESSAGE(BANK, newMsg.from, buf);
   	continue;
	} // end while
	return 0;
} // end bankTask
