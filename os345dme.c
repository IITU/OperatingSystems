// os345dme.c - Distributed Mutual Exclusion
// ***********************************************************************
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// **                                                                   **
// ** The code given here is the basis for the CS345 projects.          **
// ** It comes "as is" and "unwarranted."  As such, when you use part   **
// ** or all of the code, it becomes "yours" and you are responsible to **
// ** understand any algorithm or method presented.  Likewise, any      **
// ** errors or problems become your responsibility to fix.             **
// **                                                                   **
// ** NOTES:                                                            **
// ** -Comments beginning with "// ??" may require some implementation. **
// ** -Tab stops are set at every 3 spaces.                             **
// ** -The function API's in "OS345.h" should not be altered.           **
// **                                                                   **
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// ***********************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <assert.h>
#include "os345.h"
#include "os345dme.h"

// ***********************************************************************
// DME variables

extern Semaphore* dmeMutex;

extern Semaphore* msgReady[];
extern TCB tcb[];					// task control block
extern int curTask;				// current task #
extern Account accounts[];
extern Message messages[];		// process message buffers


// ***********************************************************************
// ***********************************************************************
// DME task
int dmeTask(int argc, char* argv[])
{
   Message newMsg;
   char buf[sizeof(Message)];

	int myDMEPort = INTEGER(argv[1]);

	while(1)
	{
		GETMESSAGE(ATMS, myDMEPort, newMsg);
		DMEDebug(("(%d->%d) %s", newMsg.from, newMsg.to, newMsg.msg));

//	?? this is where your distributed mutual exclusion algorithm (Lamports) must
//	?? be implemented.

		if (newMsg.msg[0] == 'A') { SEM_WAIT(dmeMutex); }
		if (newMsg.msg[0] == 'R') { SEM_SIGNAL(dmeMutex); }

		sprintf(buf, "OK");
		POSTMESSAGE(myDMEPort, newMsg.from, buf);
		// free message storage
		free(newMsg.msg);
	}
	return 0;
} // end dmeTask

