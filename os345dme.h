// os345dme.h - Distributed Mutual Exclusion Header
#ifndef __os345dme_h__
#define __os345dme_h__
// ***********************************************************************
// ATM/Bank equates
#define NUM_ACCOUNTS			100
#define NUM_ATMS				9
#define NUM_SEMAPHORES		30

#define CLI			0
#define ATMS		-1
#define BANK		(NUM_ATMS+1)
#define DMES		(BANK+BANK+1)

#define POSTMESSAGE(f,t,m) if(postMessage(f,t,m))SEM_SIGNAL(msgReady[t]);
#define GETMESSAGE(f,t,m) SEM_WAIT(msgReady[t]);getMessage(f,t,&m);

// bank account
typedef struct
{	int id;        				// account id
   int balance;					// account balance
} Account;

// uncomment the following to enable debug messages
//#define ATMDebug(s) printf("\n  ATM%d: ", curTask);printf s;
//#define DMEDebug(s) printf("\n  DME%d: ", curTask);printf s;
//#define BANKDebug(s) printf("\n   BANK: ");printf s;

#define ATMDebug(s) SWAP;
#define DMEDebug(s) SWAP;
#define BANKDebug(s) SWAP;

int postMessage(int from, int to, char* msg);
int getMessage(int from, int to, Message* msg);
int dmeTask(int argc, char* argv[]);

#endif // __os345dme_h__
