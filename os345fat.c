// os345fat.c - file management system
// ***********************************************************************
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// **                                                                   **
// ** The code given here is the basis for the CS345 projects.          **
// ** It comes "as is" and "unwarranted."  As such, when you use part   **
// ** or all of the code, it becomes "yours" and you are responsible to **
// ** understand any algorithm or method presented.  Likewise, any      **
// ** errors or problems become your responsibility to fix.             **
// **                                                                   **
// ** NOTES:                                                            **
// ** -Comments beginning with "// ??" may require some implementation. **
// ** -Tab stops are set at every 3 spaces.                             **
// ** -The function API's in "OS345.h" should not be altered.           **
// **                                                                   **
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// ***********************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <assert.h>
#include "os345.h"
#include "os345fat.h"

// ***********************************************************************
// fms variables
// ***********************************************************************
//
// File Allocation Tables (FAT1 & FAT2)
unsigned char FAT1[NUM_FAT_SECTORS * BYTES_PER_SECTOR];
unsigned char FAT2[NUM_FAT_SECTORS * BYTES_PER_SECTOR];

// Current directory path
char dirPath[128];

// Open File Table
FDEntry OFTable[NFILES];


// ***********************************************************************
// extern variables / functions
// ***********************************************************************
//
// RAM disk
extern unsigned char RAMDisk[SECTORS_PER_DISK * BYTES_PER_SECTOR];

extern bool diskMounted;						// disk has been mounted
extern TCB tcb[];								// task control block
extern int curTask;							// current task #

// function prototypes
extern int fmsMask(char* mask, char* name, char* ext);
extern void setDirTimeDate(DirEntry* dir);
extern int isValidFileName(char* fileName);
extern void setFatEntry(int FATindex, unsigned short FAT12ClusEntryVal);
extern unsigned short getFatEntry(int FATindex, unsigned char* FATtable);

// ***********************************************************************
// ***********************************************************************

// ***********************************************************************
// ***********************************************************************
// The fmsChangeDir function changes the current directory to the
// subdirectory specified by the argument dirname.
//
//    You will only need to move up or down a directory.
//    Verify that dirname is a valid directory name in the current directory.
//    Return 0 for success, otherwise, return the error number.
//
int fmsChangeDir(char* dirName)
{
	// ?? add code here
	printf("\nfmsChangeDir Not Implemented");

	return ERR68;
} // end fmsChangeDir


// ***********************************************************************
// ***********************************************************************
// The fmsGetNextDirEntry function returns the next directory entry of the
// current directory.
//
//    <*dirNum> should be initialized to 0 before calling fmsGetNextDirEntry.
//    fmsGetNextDirEntry updates <*dirNum>.
//    Return a 32-byte directory entry in the directory structure <*dirEntry>.
//    <*mask> is a selection string.
//       If null, return next directory entry.
//       Otherwise, use the mask string to select the next directory entry.
//       A '*' is a wild card for any length string.
//       A '?' is a wild card for any single character.
//       Any other character must match exactly.
//       NOTE:
//          *.*		all files
//          *			all files w/o extension
//          a*.txt	all files beginning with the character 'a' and with a .txt extension
//    Return 0 for success, otherwise, return the error number.
//
//    Called by dir or ls command.
//
int fmsGetNextDirEntry(int *dirNum, char* mask, DirEntry* dirEntry, int dir)
{
	// ?? add code here
	printf("\nfmsGetNextDirEntry Not Implemented");

	return ERR66;
} // end fmsGetNextDirEntry



// ***********************************************************************
// ***********************************************************************
// This function closes the open file specified by fileDescriptor.
// The fileDescriptor was returned by fmsOpenFile and is an index into the open file table.
//	Return 0 for success, otherwise, return the error number.
//
int fmsCloseFile(int fileDescriptor)
{
	// ?? add code here
	printf("\nfmsCloseFile Not Implemented");

	return ERR63;
} // end fmsCloseFile



// ***********************************************************************
// ***********************************************************************
// If attribute=DIRECTORY, this function creates a new directory
// file directoryName in the current directory.
// The directory entries "." and ".." are also defined.
// It is an error to try and create a directory that already exists.
//
// else, this function creates a new file fileName in the current directory.
// It is an error to try and create a file that already exists.
// The start cluster field should be initialized to cluster 0.  In FAT-12,
// files of size 0 should point to cluster 0 (otherwise chkdsk should report an error).
// Remember to change the start cluster field from 0 to a free cluster when writing to the
// file.
//
// Return 0 for success, otherwise, return the error number.
//
int fmsDefineFile(char* fileName, int attribute)
{
	// ?? add code here
	printf("\nfmsDefineFile Not Implemented");

	return ERR72;
} // end fmsDefineFile



// ***********************************************************************
// ***********************************************************************
// This function deletes the file fileName from the current director.
// The file name should be marked with an "E5" as the first character and the chained
// clusters in FAT 1 reallocated (cleared to 0).
// Return 0 for success; otherwise, return the error number.
//
int fmsDeleteFile(char* fileName)
{
	// ?? add code here
	printf("\nfmsDeleteFile Not Implemented");

	return ERR61;
} // end fmsDeleteFile



// ***********************************************************************
// ***********************************************************************
// This function opens the file fileName for access as specified by rwMode.
// It is an error to try to open a file that does not exist.
// The open mode rwMode is defined as follows:
//    0 - Read access only.
//       The file pointer is initialized to the beginning of the file.
//       Writing to this file is not allowed.
//    1 - Write access only.
//       The file pointer is initialized to the beginning of the file.
//       Reading from this file is not allowed.
//    2 - Append access.
//       The file pointer is moved to the end of the file.
//       Reading from this file is not allowed.
//    3 - Read/Write access.
//       The file pointer is initialized to the beginning of the file.
//       Both read and writing to the file is allowed.
// A maximum of 32 files may be open at any one time.
// If successful, return a file descriptor that is used in calling subsequent file
// handling functions; otherwise, return the error number.
//
int fmsOpenFile(char* fileName, int rwMode)
{
	// ?? add code here
	printf("\nfmsOpenFile Not Implemented");

	return ERR61;
} // end fmsOpenFile



// ***********************************************************************
// ***********************************************************************
// This function reads nBytes bytes from the open file specified by fileDescriptor into
// memory pointed to by buffer.
// The fileDescriptor was returned by fmsOpenFile and is an index into the open file table.
// After each read, the file pointer is advanced.
// Return the number of bytes successfully read (if > 0) or return an error number.
// (If you are already at the end of the file, return EOF error.  ie. you should never
// return a 0.)
//
int fmsReadFile(int fileDescriptor, char* buffer, int nBytes)
{
	// ?? add code here
	printf("\nfmsReadFile Not Implemented");

	return ERR63;
} // end fmsReadFile



// ***********************************************************************
// ***********************************************************************
// This function changes the current file pointer of the open file specified by
// fileDescriptor to the new file position specified by index.
// The fileDescriptor was returned by fmsOpenFile and is an index into the open file table.
// The file position may not be positioned beyond the end of the file.
// Return the new position in the file if successful; otherwise, return the error number.
//
int fmsSeekFile(int fileDescriptor, int index)
{
	// ?? add code here
	printf("\nfmsSeekFile Not Implemented");

	return ERR63;
} // end fmsSeekFile



// ***********************************************************************
// ***********************************************************************
// This function writes nBytes bytes to the open file specified by fileDescriptor from
// memory pointed to by buffer.
// The fileDescriptor was returned by fmsOpenFile and is an index into the open file table.
// Writing is always "overwriting" not "inserting" in the file and always writes forward
// from the current file pointer position.
// Return the number of bytes successfully written; otherwise, return the error number.
//
int fmsWriteFile(int fileDescriptor, char* buffer, int nBytes)
{
	// ?? add code here
	printf("\nfmsWriteFile Not Implemented");

	return ERR63;
} // end fmsWriteFile


// ***********************************************************************
// ***********************************************************************
int fmsUnMount(char* fileName, void* ramDisk)
// Called by the unmount command.
// This function unloads your Project 5 RAM disk image to file computer file.
// The parameter fileName is the file path name of the disk image.
// The pointer parameter ramDisk points to a character array whose size is equal to a 1.4
// mb floppy disk (2849 � 512 bytes).
// Return 0 for success; otherwise, return the error number.
{
	diskMounted = 0;							// unmount disk
	// ?? add code here
	printf("\nfmsUnMount Not Implemented");

	return -1;
} // end fmsUnMount



// ***********************************************************************
// ***********************************************************************
// Support functions
// ***********************************************************************
// ***********************************************************************
int fmsGetDirEntry(char* fileName, DirEntry* dirEntry)
// This function returns the directory entry from the current directory as
//    specified by fileName.
// Return 0 for success; otherwise, return an error number.
//
//    ERR61 = File Not Defined
{
   int error, index = 0;
	//if (isValidFileName(fileName) < 1) return ERR50;
   error = fmsGetNextDirEntry(&index, fileName, dirEntry, CDIR);
	return (error ? ((error == ERR67) ? ERR61 : error) : 0);
} // end fmsGetDirEntry


// ***********************************************************************
// ***********************************************************************
// size disk
void sizeDisk(DiskSize* dskSize, char* mask, int dir)
{
	int index = 0;
	DirEntry dirEntry;

	while (1)
	{
		if (fmsGetNextDirEntry(&index, mask, &dirEntry, dir)) break;
		if (dirEntry.name[0] == '.') continue;
		dskSize->used += ((dirEntry.fileSize + BYTES_PER_SECTOR) / BYTES_PER_SECTOR);	         // # of sectors used
		if ((dirEntry.attributes == DIRECTORY) && (dirEntry.name[0] != '.'))
		{
			sizeDisk(dskSize, mask, dirEntry.startCluster);		// get space of subdirectory
			dskSize->used += ((dirEntry.fileSize + BYTES_PER_SECTOR) / BYTES_PER_SECTOR);	         // # of sectors used
		}
	}
	return;
} // end sizeDisk


int fmsDiskStats(DiskSize* dskSize)
//	Called by sp command.
// This function returns the number of free, used, bad, and total sectors
//    on your RAM disk in the structure dskSize.
//	Return 0 for success; otherwise, return an error number.
{
	sizeDisk(dskSize, "*.*", 0);
	dskSize->free = CLUSTERS_PER_DISK - dskSize->used;	// # of sectors free
	dskSize->bad = 0;  	                        // # of bad sectors
	dskSize->size = CLUSTERS_PER_DISK;	         // Total # of sectors in RAM disk
	return 0;
} // end fmsDiskStats



// ***********************************************************************
// ***********************************************************************
int fmsMount(char* fileName, void* ramDisk)
//	Called by mount command.
// This function loads a RAM disk image from a file.
//	The parameter fileName is the file path name of the disk image.
//	The parameter ramDisk is a pointer to a character array whose
//    size is equal to a 1.4 mb floppy disk (2849 � 512 bytes).
//	Return 0 for success, otherwise, return the error number
{
   FILE* fp;
   fp = fopen(fileName, "rb");
   if (fp)
   {
      fread(ramDisk, sizeof(char), SECTORS_PER_DISK * BYTES_PER_SECTOR, fp);
   }
   else return -1;
   fclose(fp);
	// copy FAT table to memory
	memcpy(FAT1, &RAMDisk[1 * BYTES_PER_SECTOR], NUM_FAT_SECTORS * BYTES_PER_SECTOR);
	memcpy(FAT2, &RAMDisk[10 * BYTES_PER_SECTOR], NUM_FAT_SECTORS * BYTES_PER_SECTOR);
	diskMounted = 1;				// disk has been mounted
   strcpy(dirPath, fileName);
	strcat(dirPath, ":\\");
	return 0;
} // end fmsMount



// ***********************************************************************
// ***********************************************************************
int fmsReadSector(void* buffer, int sectorNumber)
//	Read into buffer RAM disk sector number sectorNumber.
// Sectors are 512 bytes.
//	Return 0 for success; otherwise, return an error number.
{
   memcpy(buffer, &RAMDisk[sectorNumber * BYTES_PER_SECTOR], BYTES_PER_SECTOR);
   return 0;
} // end fmsReadSector



// ***********************************************************************
// ***********************************************************************
int fmsWriteSector(void* buffer, int sectorNumber)
// Write 512 bytes from memory pointed to by buffer to RAM disk sector sectorNumber.
// Return 0 for success; otherwise, return an error number.
{
	memcpy(&RAMDisk[sectorNumber * BYTES_PER_SECTOR], buffer, BYTES_PER_SECTOR);
	return 0;
} // end fmsWriteSector
