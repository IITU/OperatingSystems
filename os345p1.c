// os345p1.c - Command Line Processor
// ***********************************************************************
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// **                                                                   **
// ** The code given here is the basis for the CS345 projects.          **
// ** It comes "as is" and "unwarranted."  As such, when you use part   **
// ** or all of the code, it becomes "yours" and you are responsible to **
// ** understand any algorithm or method presented.  Likewise, any      **
// ** errors or problems become your responsibility to fix.             **
// **                                                                   **
// ** NOTES:                                                            **
// ** -Comments beginning with "// ??" may require some implementation. **
// ** -Tab stops are set at every 3 spaces.                             **
// ** -The function API's in "OS345.h" should not be altered.           **
// **                                                                   **
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// ***********************************************************************
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <assert.h>
#include <time.h>
#include "os345.h"

// The 'reset_context' comes from 'main' in os345.c.  Proper shut-down
// procedure is to long jump to the 'reset_context' passing in the
// power down code from 'os345.h' that indicates the desired behavior.

extern jmp_buf reset_context;
// -----


typedef struct								// command struct
{
	char* command;
	char*	shortcut;
	int (*func)(int, char**);
	char* description;
} Command;

// ***********************************************************************
// project 1 variables
//
extern long swapCount;					// number of scheduler cycles
extern char inBuffer[];					// character input buffer
extern Semaphore* inBufferReady;		// input buffer ready semaphore
extern bool diskMounted;				// disk has been mounted
extern char dirPath[];					// directory path

Command commands[] = {
		// system
		{	"quit",			"q",		P1_quit,				"Quit"},
		{	"kill",			"kt",		P2_killTask,			"Kill task"},
		{	"reset",		"rs",		P2_reset,				"Reset system"},

		{	"project2",		"p2",		P2_project2,			"Setup project 2"},
		{	"project3",		"p3",		P3_project3,			"Setup project 3"},
		{	"project4",		"p4",		P4_project4,			"Setup project 4"},
		{	"project5",		"p5",		P5_project5,			"Pass project 5"},
		{	"project6",		"p6",		P6_project6,			"Pass project 6"},

		// project 1
		{	"help",			"he",		P1_help,				"OS345 Help"},
		{	"lc3",			"lc3",		P1_lc3,					"Execute LC3 program"},
		{	"time",			"time",		P1_time,				"Output current system date and time"},
		{	"args",			"args",		P1_args,				"list all parameters on the command line, numbers or strings"},
		{	"add",			"add",		P1_add,					"Add all numbers in command line"},
		{	"color",		"col",		P1_color,				"Set the terminal color"},
		{	"clear",		"cls",		P1_clear,				"Clear the terminal"},

		
		// project 2
		{	"semaphores",	"sem",		P2_listSems,			"List semaphores"},
		{	"tasks",		"lt",		P2_listTasks,			"List tasks"},
		{	"signal1",		"s1",		P2_signal1,				"Signal sem1 semaphore"},
		{	"signal2",		"s2",		P2_signal2,				"Signal sem2 semaphore"},

		// project 3
		{	"deltaclock",	"dc",		P3_dc,					"List deltaclock entries"},

		// project 4
		{	"atm",			"a",		P4_atm,					"Access ATM"},
		{	"messages",		"lm",		P4_listMessages,		"List messages"},
		{	"stress1",		"t1",		P4_stress1,				"ATM stress test1"},
		{	"stress2",		"t2",		P4_stress2,				"ATM stress test2"},

		// project 5
		{	"frame",		"dfm",		P5_dumpFrame,			"Dump LC-3 memory frame"},
		{	"frametable",	"dft",		P5_dumpFrameTable,		"Dump bit frame table"},
		{	"memory",		"dm",		P5_dumpLC3Mem,			"Dump LC-3 memory"},
		{	"page",			"dp",		P5_dumpPageMemory,		"Dump swap page"},
		{	"virtual",		"dvm",		P5_dumpVirtualMem,		"Dump virtual memory page"},
		{	"initmemory",	"im",		P5_initMemory,			"Initialize virtual memory"},
		{	"root",			"rpt",		P5_rootPageTable,		"Display root page table"},
		{	"user",			"upt",		P5_userPageTable,		"Display user page table"},
		{	"touch",		"vma",		P5_vmaccess,			"Access LC-3 memory location"},
		{	"stats",		"vms",		P5_virtualMemStats,		"Output virtual memory stats"},
		{	"crawler",		"cra",		P5_crawler,				"Execute crawler.hex"},
		{	"memtest",		"mem",		P5_memtest,				"Execute memtest.hex"},

		// project 6
		{	"change",		"cd",		P6_cd,					"Change directory"},
		{	"copy",			"cf",		P6_copy,				"Copy file"},
		{	"define",		"df",		P6_define,				"Define file"},
		{	"delete",		"dl",		P6_del,					"Delete file"},
		{	"directory",	"dir",		P6_dir,					"List current directory"},
		{	"mount",		"md",		P6_mount,				"Mount disk"},
		{	"mkdir",		"mk",		P6_mkdir,				"Create directory"},
		{	"run",			"run",		P6_run,					"Execute LC-3 program"},
		{	"space",		"sp",		P6_space,				"Space on disk"},
		{	"type",			"ty",		P6_type,				"Type file"},
		{	"unmount",		"um",		P6_unmount,				"Unmount disk"},

		{	"fat",			"ft",		P6_dfat,				"Display fat table"},
		{	"fileslots",	"fs",		P6_fileSlots,			"Display current open slots"},
		{	"sector",		"ds",		P6_dumpSector,			"Display disk sector"},
		{	"chkdsk",		"ck",		P6_chkdsk,				"Check disk"},
		{	"final",		"ft",		P6_finalTest,			"Execute file test"},

		{	"open",			"op",		P6_open,				"Open file test"	},
		{	"read",			"rd",		P6_read,				"Read file test"	},
		{	"write",		"wr",		P6_write,				"Write file test"	},
		{	"seek",			"sk",		P6_seek,				"Seek file test"	},
		{	"close",		"cl",		P6_close,				"Close file test"	}

		};

// ***********************************************************************
// project 1 prototypes


// ***********************************************************************
// myShell - command line interpreter
//
// Project 1 - implement a Shell (CLI) that:
//
// 1. Prompts the user for a command line.
// 2. WAIT's until a user line has been entered.
// 3. Parses the global char array inBuffer.
// 4. Creates new argc, argv variables using malloc.
// 5. Searches a command list for valid OS commands.
// 6. If found, perform a function variable call passing argc/argv variables.
// 7. Supports background execution of non-intrinsic commands.
//
int P1_shellTask(int argc, char* argv[])
{
   int i, found, newArgc;					// # of arguments
	char **newArgv;							// pointers to arguments

	while (1)
	{
		// output prompt
		if (diskMounted) printf("\n%s>>", dirPath);
		else printf("\n%ld>>", swapCount);

		SEM_WAIT(inBufferReady);			// wait for input buffer semaphore
		if (!inBuffer[0]) continue;		// ignore blank lines
		SWAP										// do context switch

		{
			// ?? >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			// ?? parse command line into argc, argv[] variables
			// ?? must use malloc for argv storage!
			static char *sp, *myArgv[MAX_ARGS];

			// init arguments
			newArgc = 1;
			myArgv[0] = sp = inBuffer;				// point to input string
			for (i=1; i<MAX_ARGS; i++)
				myArgv[i] = 0;

			// parse input string
			while ((sp = strchr(sp, ' ')))
			{
				*sp++ = 0;
				myArgv[newArgc++] = sp;
			}
			newArgv = myArgv;
		}	// ?? >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

		// look for command
		for (found=i=0; i<sizeof(commands)/sizeof(Command); i++)
		{
			if (!strcmp(newArgv[0], commands[i].command) ||
				 !strcmp(newArgv[0], commands[i].shortcut))
			{
				// command found
				found = TRUE;

				if (strcmp(newArgv[newArgc-1], "&") == 0) {
					// check for &
					
					newArgv[--newArgc] = 0;
					createTask(commands[i].command,		// task name
						commands[i].func,				// task
						MED_PRIORITY,					// task priority
						newArgc,						// task argument count
						newArgv);						// task argument pointers
					break;
				}
				else {
					int retValue = (*commands[i].func)(newArgc, newArgv);
					if (retValue) printf("\nCommand Error %d", retValue);
				}
				break;
			}
		}
		if (!found)	printf("\nInvalid command!");

		// ?? free up any malloc'd argv parameters
		for (i=0; i<INBUF_SIZE; i++) inBuffer[i] = 0;
	}
	return 0;						// terminate task
} // end P1_shellTask



// ***********************************************************************
// ***********************************************************************
// quit command
//
int P1_quit(int argc, char* argv[])
{
	// powerdown OS345
	longjmp(reset_context, POWER_DOWN_QUIT);
	return 0;
} // end P1_quit



// **************************************************************************
// **************************************************************************
// lc3 command
//
int P1_lc3(int argc, char* argv[])
{
	argv[0] = "0";
	return lc3Task(argc, argv);
//	return 0;
} // end P1_lc3



// ***********************************************************************
// ***********************************************************************
// help command
//
int P1_help(int argc, char* argv[])
{
	int commandArraySize = (int)sizeof commands / sizeof commands[0];
	int counter = 0;
	
	//printf("\n  No help here...");
	if (argc > 1) {
		int foundCommands = 0;
		while(counter<commandArraySize) {
			if (strcmp(argv[1], commands[counter].command) == 0) {
				printf("\n  %s - %s - %s", commands[counter].command, commands[counter].shortcut, commands[counter].description);
				foundCommands++;
			}
			counter++;
		}
		if (foundCommands == 0) {
			printf("\nCommand not found");
		}
	} else {
		while(counter<commandArraySize) {
			printf("\n  %s - %s - %s", commands[counter].command, commands[counter].shortcut, commands[counter].description);
			counter++;
		}
	}

	return 0;
} // end P1_help


//return current date/time
int P1_time(int argc, char *argv[]) 
{
	time_t rawtime;
	struct tm * timeinfo;

	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	printf ("\nCurrent local time and date: %s", asctime (timeinfo));

	return 0;
}

// print passed arguments
int P1_args(int argc, char *argv[])
{
	if (argc > 1) {
		int i;

		printf("\nGiven arguments: ");
		printf("%s", argv[1]);
		
		for (i=2;i<argc;i++) {
			printf(", %s", argv[i]);
		}
	} else {
		printf("\nNo argument was passed. Usage: args <arg1> <arg2> ...");
	}
	return 0;
}

// add all passed arguments
int P1_add(int argc, char *argv[]) 
{
	if (argc > 2) {
		int sum = 0;
		bool isHexadecimal = 0;
		int i;
		for (i=1;i<argc;i++) {
			if (argv[i][1] == 'X' || argv[i][1] == 'x') {
				sum += (int)strtol(argv[i], NULL, 16);
				isHexadecimal = 1;
			} else {
				int number = atoi(argv[i]);
				sum += number;
			}
		}
		printf("\nSum of all arguments: %d", sum);
		if (isHexadecimal) {
			printf("\nSum of all arguments in hex: %x", sum);
		}
	} else {
		printf("\nNot enough arguments. Usage: add <arg1> <arg2> ...");
	}
	return 0;
}

int P1_color(int argc, char *argv[])
{
	if (argc == 2) {
		char *color_code = (char*)malloc(10 * sizeof(char));
		sprintf(color_code, "%s %s", "color", argv[1]);
		printf("\nSet color to: %s", argv[1]);
		system(color_code);
	}
	else {
		printf("\nUsage: color <colorcode>");
	}
	return 0;
}

int P1_clear(int argc, char *argv[])
{
	system("cls");
	return 0;
}