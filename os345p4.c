// os345p4.c - Distributed Mutual Exclusion
// ***********************************************************************
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// **                                                                   **
// ** The code given here is the basis for the CS345 projects.          **
// ** It comes "as is" and "unwarranted."  As such, when you use part   **
// ** or all of the code, it becomes "yours" and you are responsible to **
// ** understand any algorithm or method presented.  Likewise, any      **
// ** errors or problems become your responsibility to fix.             **
// **                                                                   **
// ** NOTES:                                                            **
// ** -Comments beginning with "// ??" may require some implementation. **
// ** -Tab stops are set at every 3 spaces.                             **
// ** -The function API's in "OS345.h" should not be altered.           **
// **                                                                   **
// **   DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER ** DISCLAMER   **
// ***********************************************************************
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <setjmp.h>
#include <assert.h>
#include "os345.h"
#include "os345dme.h"

// ***********************************************************************
// project 4 variables

Semaphore* dmeMutex;
Semaphore* msgReady[NUM_SEMAPHORES];
Semaphore* dmeMutex;

extern TCB tcb[];					// task control block
extern int curTask;				// current task #
extern Account accounts[];
extern Message messages[];		// process message buffers


// ***********************************************************************
// ***********************************************************************
// project 4 functions and tasks
// *****************************************************************************
// project4 command
//
//						atms     myCLIPort		BANK
//												(10) BANK
//		CLI (0)		(1) ATM1 (11)							(21) DME1
//						(2) ATM2 (12)							(22) DME2
//							....										....
//						(9) ATM9 (19)							(29) DME9
//
int P4_project4(int argc, char* argv[])					// project 4
{
	int i;
	char buf[MAX_MESSAGE_SIZE];
	char* newArgv[2];

	// kill all tasks
	killTask(-1);

	// initialize accounts
	for (i=0; i<NUM_ACCOUNTS; i++)
	{
		accounts[i].id = 0;
	}

	// initalize message buffers
	for (i=0; i<NUM_MESSAGES; i++)
	{
		messages[i].to = -1;
	}

	// create message buffer semaphores
	for (i=0; i<NUM_SEMAPHORES; i++)
	{
		sprintf(buf, "Port[%02d]", i);
		msgReady[i] = createSemaphore(buf, 1, 0);
	}

	// create dme mutex
	dmeMutex = createSemaphore("dmeMutex", 1, 1);

	// startup atm tasks (ports 1-9) w/priority 1 less than CLI
	for (i=0; i<NUM_ATMS; i++)
	{
		sprintf(buf, "ATM #%d", i+1);
		newArgv[0] = buf;
		newArgv[1] = buf+5;
		createTask( buf,				// task name
						atmTask,			// task
						LOW_PRIORITY,	// task priority
						2,					// task argc
						newArgv );		// task argv
	}

	// startup bank (port 10) w/priority 1 greater than CLI
	strcpy(buf, "USA Bank");
	newArgv[0] = buf;
	createTask( "USA Bank",			// task name
					bankTask,			// task
					HIGH_PRIORITY,		// task priority
					1,						// task argc
					newArgv);			// task argv

	// startup DME tasks (ports 11-19) to enforce distributed mutual exclusion
	for (i=0; i<NUM_ATMS; i++)
	{
		sprintf(buf, "DME #%d", i + DMES);	// (21-29)
		newArgv[0] = buf;
		newArgv[1] = buf+5;
		createTask( buf,				// task name
						dmeTask,			// task
						VERY_HIGH_PRIORITY,	// task priority
						2,					// task argc
						newArgv);		// task argv
	}

	// OK to access accounts now
	// create an initial account
#define ATM1	1

	printf("\nCreate new account 1000 with $10000");
	sprintf(buf, "n,1000,10000");
	POSTMESSAGE(CLI, ATM1, buf);

	printf("\nCreate new account 2000 with $20000");
	sprintf(buf, "n,2000,20000");
	POSTMESSAGE(CLI, ATM1, buf);

	printf("\nBank Account Summary:");
	sprintf(buf, "t,9999");
	POSTMESSAGE(CLI, ATM1, buf);

	return 0;
} // end P4_project4



// ***********************************************************************
// **************************************************************************
// ATM 1,1111,b         ; account balance
// ATM 1,1111,d,100     ; deposit $100 to 1111
// ATM 1,1111,n,100     ; open new account
// ATM 1,9999,t         ; bank totals
// ATM 1,1111,w,100     ; withdraw $100 from 1111
// ATM 1,1111,x         ; delete account
int P4_atm(int argc, char* argv[]) 	   // ATM
{
	char buf[MAX_MESSAGE_SIZE];
	char code;
	int atm, account, amount;

	if (argc < 4)
	{
		printf("\n  ATM <atm #>,<account>,<code>{,<amount>}");
		printf("\n      <atm #> = 1-9");
 		printf("\n       <code> = b  account balance");
 		printf("\n                d  deposit");
  		printf("\n                n  new account");
  		printf("\n                t  bank totals");
  		printf("\n                w  withdrawal");
  		printf("\n                x  delete account");
  		return 0;
	}

	// set atm account, code, and amount variables
	atm = INTEGER(argv[1]);
	account = INTEGER(argv[2]);
	code = argv[3][0];
	amount = (argc < 5) ? 0 : INTEGER(argv[4]);
	if ((atm < 1) || (atm > NUM_ATMS))
	{
		printf("\n  **Illegal ATM");
		return 0;
	}

	// send message to atm
	sprintf(buf, "%c,%d,%d", code, account, amount);
	printf("\n    CLI: Send to ATM%d '%s'", atm, buf);
	POSTMESSAGE(CLI, atm, buf);
	return 0;
} // end P4_atm



// ***********************************************************************
// **************************************************************************
int P4_listMessages(int argc, char* argv[])
{
	int i;
	for (i=0; i<NUM_MESSAGES; i++)
	{
		if (messages[i].to != -1)
		{
			printf("\n  Message[%d] %d->%d:%s",
				i, messages[i].from, messages[i].to, messages[i].msg);
		}
		SWAP;
	}
	return 0;
} // end P4_listMessages



// ***********************************************************************
// **************************************************************************
int P4_stress1(int argc, char* argv[])		// stress1
{
	printf("\nCreate new account 1111 with $111");
	POSTMESSAGE(CLI,1,"n,1111,111");

	printf("\nCreate new account 2222 with $222");
	POSTMESSAGE(CLI,2,"n,2222,222");

	printf("\nDeposit $2000 to account 1111");
	POSTMESSAGE(CLI,1,"d,1111,2000");

	printf("\nWithdraw $22 from account 2222");
	POSTMESSAGE(CLI,2,"w,2222,22");

	printf("\nRequest balance of account 1111");
	POSTMESSAGE(CLI,1,"b,1111");

	printf("\nWithdraw $1111 from account 1111");
	POSTMESSAGE(CLI,1,"w,1111,1111");

	printf("\nRequest balance of account 1111");
	POSTMESSAGE(CLI,1,"b,1111");

	printf("\nRequest balance of account 2222");
	POSTMESSAGE(CLI,2,"b,2222");

	printf("\nDeposit $1800 to account 222");
	POSTMESSAGE(CLI,2,"d,2222,1800");

	printf("\nRequest balance of account 2222");
	POSTMESSAGE(CLI,2,"b,2222");

	printf("\n  Account Summary:");
	POSTMESSAGE(CLI,1,"t,9999");

	P4_listMessages(argc, argv);
	return 0;
} // end P4_stress1



// ***********************************************************************
// **************************************************************************
int P4_stress2(int argc, char* argv[])		// stress2
{
	char buf[MAX_MESSAGE_SIZE];
	int a, atm;

#define startAccount 1001
#define endAccount   1009

	//define accounts 1001 thru 1009 with $100 in each
	for (atm=1,a=startAccount; a<=endAccount; atm++,a++)
	{
		sprintf(buf, "%c,%d,%d", 'n', a, 100);
		POSTMESSAGE(CLI,atm,buf);
	}

	// deposit $9 to 1st account, $18 to 2nd, ...
	for (atm=1; atm<10; atm++)
	{
		for (a=startAccount; a<=endAccount; a++)
		{
			sprintf(buf, "%c,%d,%d", 'd', a, a-startAccount+1);
			POSTMESSAGE(CLI,atm,buf);
		}
	}

	// withdraw $8 from 1st, $16 from 2nd, ...
	for (atm=1,a=startAccount; a<=endAccount; atm++,a++)
	{
		sprintf(buf, "%c,%d,%d", 'w', a, (a-startAccount+1)*8);
		POSTMESSAGE(CLI,atm,buf);
	}

	// withdrawal $100 from each account
	for (atm=1,a=startAccount; a<=endAccount; atm++,a++)
	{
		sprintf(buf, "%c,%d,%d", 'w', a, 100);
		POSTMESSAGE(CLI,atm,buf);
	}

	// examine results thru atms
	for (atm=1,a=startAccount; a<=endAccount; atm++,a++)
	{
		sprintf(buf, "%c,%d", 'b', a);
		POSTMESSAGE(CLI,atm,buf);
	}

	// get bank summary
	POSTMESSAGE(CLI,1,"t,9999");

	P4_listMessages(argc, argv);
	return 0;
} // end P4_stress2
